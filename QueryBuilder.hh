<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder
{
	require_once('types.hh');
	
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin,
		loader\ClassLoaderException
	};
	use nuclio\plugin\database\
	{
		exception\DatabaseException,
		datasource\source\Source as DataSource,
		orm\Model,
		orm\DynamicModel,
		orm\Cursor,
		common\CommonCursorInterface,
		common\DBQuery,
		common\DBOrder
	};
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	
	<<factory>>
	class QueryBuilder extends Plugin
	{
		private DataSource $boundDataSource;
		private CommonQueryBuilderInterface $driver;
		private Map<string,Model> $modelCache	=Map{};
		private ?Model $target					=null;
		private Joins $joins					=Map{};
		private DBQuery $filter					=Map{};
		private int $limit						=100;
		private int $offset						=0;
		private DBOrder $orderBy				=Map{};
		private ?string $lastQuery				=null;
		
		public static function getInstance(DataSource $source,?string $provider=null):QueryBuilder
		{
			$instance=ClassManager::getClassInstance(self::class,$source,$provider);
			return ($instance instanceof self)?$instance:new self($source,$provider);
		}
		
		public function __construct(DataSource $source,?string $provider=null)
		{
			parent::__construct();
			$this->boundDataSource=$source;
			$driverName=null;
			if (is_null($provider))
			{
				$driverName	=$this->boundDataSource->getDriverName();
				$provider	='queryBuilder::'.$driverName;
			}
			$driverInstance=ProviderManager::request($provider,$this,$source);
			if ($driverInstance instanceof CommonQueryBuilderInterface)
			{
				$this->driver=$driverInstance;
			}
			else
			{
				throw new DatabaseException(sprintf('Unable to load driver for queryBuilder. Driver "%s" is not available.',$driverName));
			}
		}
		
		public function setTarget(string $target):this
		{
			$cacheIt=true;
			if ($this->modelCache->containsKey($target))
			{
				$model=$this->modelCache->get($target);
				if ($model instanceof Model)
				{
					$this->modelCache->set($target,$model);
					$cacheIt=false;
				}
				else
				{
					throw new DatabaseException(sprintf('Unexpected target. Target should be a valid Model. Got "%s" as the target. BUT THIS SHOULD NOT HAVE HAPPENED!!!',$target));
				}
			}
			try
			{
				$model=ClassManager::getClassInstance($target);
			}
			catch (ClassLoaderException $exception)
			{
				if (!strstr($target,'/'))
				{
					$model=new DynamicModel();
					$model->setCollection($target);
				}
				else
				{
					throw $exception;
				}
			}
			if ($model instanceof Model)
			{
				if ($cacheIt)
				{
					$this->modelCache->set($target,$model);
				}
				$this->target=$model;
			}
			else
			{
				throw new DatabaseException(sprintf('Unexpected target. Target should be a valid Model. Got "%s" as the target.',$target));
			}
			return $this;
		}
		
		public function setFilter(DBQuery $filter):this
		{
			$this->filter=$filter;
			return $this;
		}
		
		public function setLimit(int $limit):this
		{
			$this->limit=$limit;
			return $this;
		}
		
		public function setOffset(int $offset):this
		{
			$this->offset=$offset;
			return $this;
		}
		
		public function setOrderBy(DBOrder $orderBy):this
		{
			$this->orderBy=$orderBy;
			return $this;
		}
		
		public function execute():Cursor
		{
			$target=$this->target;
			if (!is_null($target))
			{
				$cursor=$this->driver->execute
				(
					shape
					(
						'target'	=>$target->getCollection(),
						'filter'	=>$this->filter,
						'limit'		=>$this->limit,
						'offset'	=>$this->offset,
						'orderBy'	=>$this->orderBy,
						'joins'		=>$this->joins
					)
				);
				return Cursor::getInstance($target,$cursor);
			}
			else
			{
				throw new DatabaseException('Unable to execute query. No target has been bound.');
			}
		}
		
		public function parseFilter():WheresAndValues
		{
			$target=$this->target;
			if (!is_null($target))
			{
				return $this->driver->parseFilter
				(
					$target->getCollection(),
					$this->filter
				);
			}
			else
			{
				throw new DatabaseException('Unable to execute query. No target has been bound.');
			}
		}
		
		public function setLastQuery(string $lastQuery):this
		{
			$this->lastQuery=$lastQuery;
			return $this;
		}
		
		public function getLastQuery():?string
		{
			return $this->lastQuery;
		}
	}
}
