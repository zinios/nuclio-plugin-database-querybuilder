<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder
{
	use nuclio\plugin\database\common\
	{
		DBOrder,
		DBQuery
	};
	
	type Joins=Map<string,string>;
	type ExecutionParams=shape
	(
		'target'	=>string,
		'filter'	=>DBQuery,
		'limit'		=>int,
		'offset'	=>int,
		'orderBy'	=>DBOrder,
		'joins'		=>Joins
	);
	type Wheres				=Vector<string>;
	type Values				=Vector<string>;
	type WheresAndValues	=Pair<Wheres,Values>;
}
