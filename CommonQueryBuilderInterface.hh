<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\queryBuilder
{
	use nuclio\plugin\database\
	{
		queryBuilder\CommonQueryBuilderInterface,
		queryBuilder\ExecutionParams,
		queryBuilder\WheresAndValues,
		common\DBQuery,
		common\CommonCursorInterface
	};
	
	interface CommonQueryBuilderInterface
	{
		public function execute(ExecutionParams $params):CommonCursorInterface;
		
		public function parseFilter(string $target, DBQuery $filter):WheresAndValues;
	}
}
